
# Prometheus-Metrics-PACKAGE

# Laravel

## Composer

ADD this to your ```composer.json```

```
"repositories": [
        {
            "type": "git",
            "url": "git@gitlab.jtl-software.com:idev/global-composer-packages/prometheusmetrics.git",
            "reference": "origin/master"
        }
],
"require": {
          "idev/prometheusmetrics": "^1.0"
},
```

OR

```
"repositories": [
        {
            "type": "git",
            "url": "https://gitlab.jtl-software.com/idev/global-composer-packages/prometheusmetrics.git",
            "reference": "origin/master"
        }
],
"require": {
       "idev/prometheusmetrics": "^1.0"
},
```

## Route protection with middleware

### Attach the middleware to a route
```
Route::get('/', 'IndexController@index')->middleware('jwt.user');
```

### To restrict access to the route to specific scopes you may provide a comma-delimited list of the required scopes
```
Route::get('/', 'IndexController@index')->middleware(['jwt.user','jwt.scopes:profile|email|address');
```

### The client credentials middleware is suitable for machine-to-machine authentication
```
Route::get('/', 'IndexController@index')->middleware('jwt.service');
```

## Laravel 7 - optional
Add middlewares above auth in priority list ```App\Http\Kernel.php```

```
    /**
     * The priority-sorted list of middleware.
     * This forces non-global middleware to always be in the given order.
     *
     * @var array
     */
    protected $middlewarePriority = [
        ...
        \Jtl\JwtApiPackage\Http\Middleware\JwtUserMiddleware::class,
        \Jtl\JwtApiPackage\Http\Middleware\JwtServiceMiddleware::class,
        \Jtl\JwtApiPackage\Http\Middleware\CheckScopeMiddleware::class,
        \App\Http\Middleware\Authenticate::class,
        ...
    ];
```

## Route protection for users with guard

### Step 1 : register guard

Add guard in    ```app/Providers/AuthServiceProvider.php``` file.


```
<?php

    /**
     * Register any authentication / authorization services.
     *
     * @return void
    */
    public function boot()
    {
        $this->registerPolicies();

         // Register the JWT Guard
        Auth::extend('jwt-jtl', function ($app, $name, array $config) {
            return new JwtGuard(Auth::createUserProvider($config['provider']), $app['request']);
        });
    }
```

### Step 2 : set auth settings

Add api driver in   ```config/auth.php``` file.


```
<?php

        'guards' => [
  

        'jwtapi' => [
            'driver' => 'jwt-jtl',
            'provider' => 'users',
            'hash' => false,
        ],
    ],

```

### Step 3 : use on routes 

Use 'auth:api' middleware in  ```routes/api.php``` file.


```
<?php

    Route::middleware('auth:jwtapi')->get('/', function (Request $request) {
    dump($request);
});


```



