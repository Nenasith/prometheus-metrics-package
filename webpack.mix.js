let {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Dashboard-Shopping-List-Package
 | Created by janweskamp on 21.07.17.
 |--------------------------------------------------------------------------
 */

//COMPILE SASS TO CSS
/*

mix.sass(
    'src/resources/assets/sass/schemas/sl-admin.scss',
    'src/public/css/shopping_list/backend/'
);
*/


//COMPILE JS FILES
/*mix.js([
        'src/resources/assets/js/schemas/sl-admin.js'
    ], 'src/public/js/shopping_list/backend/'
).js([
        'src/resources/assets/js/schemas/sl-frontend.js'
    ], 'src/public/js/shopping_list/frontend/'
).js(
    [
        'src/resources/assets/js/components/user_2_columns.js'
    ],
    'src/public/js/shopping_list/backend/user_management.js'
);*/

//COMPILE VUE JS
/*mix.js(
    [
        'src/resources/assets/js/vue/backend-components.js'
    ],
    'src/public/js/shopping_list/backend/vue-components.js'
).js(
    [
        'src/resources/assets/js/vue/frontend-components.js'
    ],
    'src/public/js/shopping_list/frontend/vue-components.js'
);*/

//COMBINE CSS
/*
 mix.styles(
 [
 'src/public/css/survey_system/backend/create_edit_appointment-datepicker.css',
 'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css'
 ],
 'src/public/css/survey_system/backend/create_edit_appointment.css'
 );*/
