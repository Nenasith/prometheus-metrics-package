<?php


namespace App\Exporters;

use Laravel\Horizon\Contracts\JobRepository;
use Jtl\PrometheusMetrics\Generic\Contracts\Exporter;
use Prometheus\CollectorRegistry;

class HorizonFailedJobsPerHour implements Exporter
{
    protected $gauge;

    public function metrics(CollectorRegistry $collectorRegistry)
    {
        $this->gauge = $collectorRegistry->registerGauge(
            config('jtl-prometheusmetrics.application.key'),
            'horizon_failed_jobs',
            'The number of recently failed jobs'
        );
    }

    public function collect()
    {
        $this->gauge->set(app(JobRepository::class)->countRecentlyFailed());
    }
}
