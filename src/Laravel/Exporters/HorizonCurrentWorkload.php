<?php


namespace App\Exporters;

use Jtl\PrometheusMetrics\Generic\Contracts\Exporter;
use Laravel\Horizon\Contracts\WorkloadRepository;
use Prometheus\CollectorRegistry;

class HorizonCurrentWorkload implements Exporter
{
    protected $gauge;

    public function metrics(CollectorRegistry $collectorRegistry)
    {
        $this->gauge = $collectorRegistry->registerGauge(
            config('jtl-prometheusmetrics.application.key'),
            'horizon_current_workload',
            'Current workload of all queues',
            ['queue']
        );
    }

    public function collect()
    {
        $workloadRepository = app(WorkloadRepository::class);
        $workloads = collect($workloadRepository->get())->sortBy('name')->values();

        $workloads->each(function ($workload) {
            $this->gauge->set($workload['length'], [$workload['name']]);
        });
    }
}
