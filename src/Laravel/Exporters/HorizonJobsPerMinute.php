<?php


namespace App\Exporters;

use Laravel\Horizon\Contracts\MetricsRepository;
use Jtl\PrometheusMetrics\Generic\Contracts\Exporter;
use Prometheus\CollectorRegistry;

class HorizonJobsPerMinute implements Exporter
{
    protected $gauge;
    public function metrics(CollectorRegistry $collectorRegistry)
    {
        $this->gauge = $collectorRegistry->registerGauge(
            config('jtl-prometheusmetrics.application.key'),
            'horizon_jobs_per_minute',
            'The number of jobs per minute'
        );
    }

    public function collect()
    {
        $this->gauge->set(app(MetricsRepository::class)->jobsProcessedPerMinute());
    }
}
