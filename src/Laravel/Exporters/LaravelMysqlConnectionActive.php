<?php


namespace App\Exporters;


use Illuminate\Support\Facades\DB;
use Jtl\PrometheusMetrics\Generic\Contracts\Exporter;
use Prometheus\CollectorRegistry;

class LaravelMysqlConnectionActive implements Exporter
{
    protected $gauge;

    public function metrics(CollectorRegistry $collectorRegistry)
    {
        $this->gauge = $collectorRegistry->registerGauge(
            config('jtl-prometheusmetrics.application.key'),
            'horizon_status',
            'The status of MySql, -1 = inactive, 0 = paused, 1 = running'
        );
    }

    public function collect()
    {
        try {
            if (DB::connection()->getDatabaseName()) {
                $status = 1;
            } else {
                $status = 0;
            }
        }catch (\Exception $e){
            $status = -1;
        }

        $this->gauge->set($status);
    }
}
