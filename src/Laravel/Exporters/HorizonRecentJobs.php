<?php


namespace App\Exporters;

use aravel\Horizon\Contracts\JobRepository;
use Jtl\PrometheusMetrics\Generic\Contracts\Exporter;
use Prometheus\CollectorRegistry;

class HorizonRecentJobs implements Exporter
{
    protected $gauge;

    public function metrics(CollectorRegistry $collectorRegistry)
    {
        $this->gauge = $collectorRegistry->registerGauge(
            config('jtl-prometheusmetrics.application.key'),
            'horizon_recent_jobs',
            'The number of recent jobs'
        );
    }

    public function collect()
    {
        $this->gauge->set(app(JobRepository::class)->countRecent());
    }
}
