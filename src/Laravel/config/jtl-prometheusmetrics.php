<?php
return [
    "application_key" => 'app',

    "enabled"            => env('PROMETHEUS_METRICS_ENABLED', true),

    /**
     * You can change the default endpoint to something other than metrics.
     * Keep in mind that the change needs to be reflected in your Prometheus configuration as well.
     */
    "url"                => 'metrics',

    /**
     * You can disable exporters by simply add them here.
     */
    "disabled-exporters" => [
        '\App\Exporter\HorizonCurrentProccesesPerQueue',
        '\App\Exporter\HorizonCurrentWorkload',
        '\App\Exporter\HorizonFailedJobsPerHour',
        '\App\Exporter\HorizonJobsPerMinute',
        '\App\Exporter\HorizonRecentJobs',
        '\App\Exporter\HorizonStatus',
    ],

    /**
     * IP Whitelisting, you may don't want to expose your metrics on the internet so you can add the IP addresses of your Prometheus Server here .
     */
    "ip_whitelist"       => [
        // Keep empty to allow all IP addresses
    ],

    /**
     * You can change the Middleware which is used for the IP whitelisting.  You can add your own, like a token based authentication.
     */
    "middleware"         => \Jtl\PrometheusMetrics\Laravel\Http\Middleware\ExporterIPWhitelistingMiddleware::class,
];
