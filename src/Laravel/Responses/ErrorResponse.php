<?php

namespace Jtl\JwtApiPackage\Responses;

use Illuminate\Http\JsonResponse;

class ErrorResponse extends JsonResponse
{
    /**
     * ErrorResponse constructor.
     * @param null $data
     * @param int $status
     * @param array $headers
     * @param int $options
     */
    public function __construct($data = null, $status = 500, $headers = [], $options = 0)
    {
        $this->encodingOptions = $options;
        
        parent::__construct($data, $status, $headers);
    }
    
    /**
     * @param array $data
     * @return JsonResponse|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function setData($data = [])
    {
        $data = [
            'message' => $data
        ];
        
        return parent::setData($data);
    }
}
