<?php

namespace Jtl\Prometheusmetrics\Laravel;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Jtl\PrometheusMetrics\Laravel\Http\Middleware\ExporterIPWhitelistingMiddleware;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        require __DIR__ . '/routes/routes.php';

        /* $this->loadViewsFrom(__DIR__ . '/resources/views', 'placeholder');*/
        /*  $this->loadMigrationsFrom(__DIR__ . '/database/migrations');*/


        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/config/jtl-prometheusmetrics.php' => config_path('jtl-prometheusmetrics.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/Exporters' => app_path('Exporters'),
            ], 'exporter');
        }
        /*
            $this->publishes([
                __DIR__ . '/public/img/' => public_path('img'),
            ], 'img');

            $this->publishes([
                __DIR__ . '/public/js/' => public_path('js'),
            ], 'js');

            $this->publishes([
                __DIR__ . '/public/css/' => public_path('css'),
            ], 'css');*/

       //$this->aliasMiddleware();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/jtl-prometheusmetrics.php', 'jtl-prometheusmetrics');

        include __DIR__ . '/routes/routes.php';
        $this->registerCommands();

/*        $this->commands([
            'jwt.api.get.crt',
        ]);*/

       /* $this->app->bind('JtlJwtToken', function () {
            return new JtlJwtTokenController();
        });*/

        $this->registerFacades();
    }


    /**
     * Register the Artisan command.
     *
     * @return void
     */
    protected function registerCommands()
    {
       /* $this->app->singleton('jwt.api.get.crt', function () {
            return new GetCrtCommand;
        });*/
    }

    /**
     * Register the Facades
     *
     * @return void
     */
    protected function registerFacades()
    {
        /*AliasLoader::getInstance()->alias('JtlJwtToken', 'Jtl\JwtApiPackage\Facades\JtlJwtToken');*/
    }

    /**
     * Alias the middleware.
     *
     * @return void
     */
    protected function aliasMiddleware()
    {
        $router = $this->app['router'];

        $method = method_exists($router, 'aliasMiddleware') ? 'aliasMiddleware' : 'middleware';

        foreach ($this->middlewareAliases as $alias => $middleware) {
            $router->$method($alias, $middleware);
        }
    }


    /**
     * The middleware aliases.
     *
     * @var array
     */
    protected $middlewareAliases = [
      /*  'jwt.service' => JwtServiceMiddleware::class,
        'jwt.user'    => JwtUserMiddleware::class,
        'jwt.scopes'  => CheckScopeMiddleware::class,*/
      'jtl.ip.prometheus.exporter' => ExporterIPWhitelistingMiddleware::class,
    ];

}
