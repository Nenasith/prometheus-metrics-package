<?php


namespace Jtl\PrometheusMetrics\Laravel\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use GuzzleHttp\Psr7\Response;
use Jtl\PrometheusMetrics\Generic\Repository\ExporterRepository;
use Prometheus\CollectorRegistry;
use Prometheus\RenderTextFormat;

class PrometheusMetricsController extends BaseController
{
    public function metrics()
    {
        $exporters = [];
        $disabledExporters = config('jtl-prometheusmetrics.disabled-exporters');

        if (is_dir(app_path('Exporters'))) {
            $files = array_diff(
                scandir(app_path('Exporters')),
                [
                    '..',
                    '.',
                ]
            );

            foreach ($files as $fileKey => $fileName) {
                $fileNameParts = explode('.', $fileName);

                $exporter = sprintf('\App\Exporters\%s', $fileNameParts[0]);
                $test = in_array($exporter, $disabledExporters);


                if (!$test) {
                    $exporters[] = sprintf('%s%s', $exporter, '::class');
                }
            }
        }
        ExporterRepository::load($exporters);
        $renderer = new RenderTextFormat();
        /** @var CollectorRegistry $registry */
        $registry = ExporterRepository::getRegistry();
        $result = 'No metrics availabale';
        if($registry instanceof CollectorRegistry){
            $result = $renderer->render($registry->getMetricFamilySamples());
        }

        return new Response(
            200,
            [
                "Content-Type" => RenderTextFormat::MIME_TYPE,
            ],
            $result
        );
    }
}
