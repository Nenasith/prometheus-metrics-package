<?php


namespace Jtl\PrometheusMetrics\Laravel\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\IpUtils;

class ExporterIPWhitelistingMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
        if (!empty(config('jtl-prometheusmetrics.ip_whitelist'))) {
            $clientIp = $request->ip();
            if (IpUtils::checkIp($clientIp, config('jtl-prometheusmetrics.ip_whitelist'))) {
                return $next($request);
            } else {
                abort(403);
            }
        } else {
            return $next($request);
        }
    }
}
