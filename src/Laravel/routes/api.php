<?php

Route::get(
    config('jtl-prometheusmetrics.url'),
    \Jtl\PrometheusMetrics\Laravel\Http\Controllers\PrometheusMetricsController::class.'@metrics'
);

